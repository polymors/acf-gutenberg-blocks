<?php

namespace Syneforge\AcfBlocks;

/**
 * Description.
 * class AcfGutenbergBlock is used for register acf block type.
 *
 * @author polymors@syneforge.com
 */
class AcfGutenbergBlock
{
    public $name;
    public $title;
    public $category;
    public $template_file;
    public $template_url;
    public $template_dir;
    public $assets_dir;
    public $enqueue_style;
    public $enqueue_script;

    public function __construct()
    {
        $this->category = 'widgets';
        $this->template_url = defined(TEMPLATE_URL) ? TEMPLATE_URL : get_template_directory_uri();
        $this->template_dir = defined(SERVER_PATH_TEMPLATE) ? SERVER_PATH_TEMPLATE : get_template_directory();
        $this->assets_dir = $this->template_dir . '/assets/dist';
    }

    // Get template file for registered block
    public function render_callback($block): void
    {
        if (!$this->template_file) {
            $this->template_file = __DIR__ . '/templates/' . $this->name . '/' . $this->name . '-template.php';
        }

        $check_template_file = $this->check_file($this->template_file);

        if (true === $check_template_file) {
            include_once $this->template_file;
        } else {
            $this->error_notice($check_template_file);
        }
    }

    // Register block
    public function register_block(): void
    {
        add_action('acf/init', function () {
            if (function_exists('acf_register_block_type')) {
                $attr = [
                    'name'            => $this->name,
                    'title'           => $this->title,
                    'render_callback' => [ $this, 'render_callback' ],
                    'category'        => $this->category,
                    'icon'            => 'tagcloud',
                    'keywords'        => [$this->name],
                ];

                if (true === $this->enqueue_style) {
                    $attr['enqueue_style'] = $this->template_url . '/assets/dist/css/' . $this->name . '.css';
                } elseif (true == $this->enqueue_style) {
                    $attr['enqueue_style'] = $this->enqueue_style;
                }

                if (true === $this->enqueue_script) {
                    $attr['enqueue_script'] = $this->template_url . '/assets/dist/js/' . $this->name . '.js';
                } elseif (true == $this->enqueue_style) {
                    $attr['enqueue_script'] = $this->enqueue_script;
                }

                acf_register_block_type($attr);
            }
        });
    }

    // Check file for exists
    protected function check_file($file)
    {
        try {
            if (file_exists($file)) {
                return true;
            }

            throw new \Exception(
                sprintf(
                        'Template file <b>%s</b> for block is not exists!!!<br>Please add it.',
                        $file
                    )
            );
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }

    protected function error_notice($notice)
    {
        if (is_admin()) {
            echo $notice;
        }
    }
}
