<?php

include_once 'AcfGutenbergBlock.php';

use Syneforge\AcfBlocks\AcfGutenbergBlock;

$blocks_for_register = [
    'content-banner' => [
        'title' => 'EXT Content banner',
        'style' => true,
    ],
    'bonus-table' => [
        'title' => 'EXT Bonus table',
        'style' => true,
    ],
    'quick-links' => [
        'title' => 'EXT Quick links',
        'style' => true,
    ],
    'wide-button' => [
        'title' => 'EXT Wide button',
        'style' => true,
    ],
    'faq' => [
        'title'  => 'EXT FAQ',
        'style'  => true,
        'script' => true,
    ],
    'test' => [
        'title' => 'EXT test',
        'style' => true,
    ],
];

foreach ($blocks_for_register as $key => $val) {
    $block = new AcfGutenbergBlock();
    $block->name = $key;
    $block->title = $val['title'];
    $block->enqueue_style = $val['style'] ?? false;
    $block->enqueue_script = $val['script'] ?? false;
    $block->register_block();
    unset($block);
}
